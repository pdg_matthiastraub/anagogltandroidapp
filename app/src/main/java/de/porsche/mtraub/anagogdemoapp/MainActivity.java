package de.porsche.mtraub.anagogdemoapp;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

import javax.net.ssl.HttpsURLConnection;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        JSONObject eventData = new JSONObject();
        try {
            eventData.put("meta_state", "teststate");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        RequestPerformer requestPerformer = new RequestPerformer();
        requestPerformer.execute(eventData.toString());

        Intent intent = new Intent("anagog.pd.service.MobilityService");
        intent.setClassName(getPackageName(), "anagog.pd.service.MobilityService");
        startService(intent);
    }
}
