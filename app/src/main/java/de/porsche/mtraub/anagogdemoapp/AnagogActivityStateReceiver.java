package de.porsche.mtraub.anagogdemoapp;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import org.json.JSONException;
import org.json.JSONObject;

import anagog.pd.service.api.metastate.MetaStateData;
import anagog.pd.service.api.metastate.MetaStateManager;
import anagog.pd.service.api.userstate.UserState;
import anagog.pd.service.api.userstate.activity.ActivityUserStateData;
import anagog.pd.service.api.userstate.activity.UserStateActivityType;

/**
 * Created by p367151 on 21.02.18.
 */

public class AnagogActivityStateReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {

        JSONObject eventData = new JSONObject();

        try {
            Bundle extras = intent.getExtras();
            if (extras != null) {
                eventData.put("lat", extras.getDouble("Lat"));
                eventData.put("long", extras.getDouble("Long"));
                eventData.put("accuracy", extras.getFloat("Accuracy"));
                eventData.put("duration", extras.getLong("Duration"));
            }

            eventData.put("driving_action_type", UserState.ANAGOG_USER_STATE_ACTIVITY);

            ActivityUserStateData data = UserState.extractActivityUserState(intent);
            // Get the activity type (Driving, Running)
            UserStateActivityType type = UserState.extractUserStateActivityType(intent);
            eventData.put("userStateActivityType", type.toString());
            // Get the confidence level
            eventData.put("confidence", data.getConfidenceLevel());
            eventData.put("category", data.getCategory());
        } catch (JSONException e) {
            e.printStackTrace();
        }


        RequestPerformer requestPerformer = new RequestPerformer();
        requestPerformer.execute(eventData.toString());
    }
}
