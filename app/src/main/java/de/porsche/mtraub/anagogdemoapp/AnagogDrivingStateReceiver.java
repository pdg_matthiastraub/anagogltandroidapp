package de.porsche.mtraub.anagogdemoapp;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import org.json.JSONException;
import org.json.JSONObject;

import anagog.pd.service.api.metastate.MetaStateData;
import anagog.pd.service.api.metastate.MetaStateManager;

/**
 * Created by p367151 on 20.02.18.
 */

public class AnagogDrivingStateReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {

        JSONObject eventData = new JSONObject();

        try {
            Bundle extras = intent.getExtras();
            if (extras != null) {
                eventData.put("lat", extras.getDouble("Lat"));
                eventData.put("long", extras.getDouble("Long"));
                eventData.put("accuracy", extras.getFloat("Accuracy"));
                eventData.put("duration", extras.getLong("Duration"));
            }

            eventData.put("driving_action_type", intent.getAction());
        } catch (JSONException e) {
            e.printStackTrace();
        }


        RequestPerformer requestPerformer = new RequestPerformer();
        requestPerformer.execute(eventData.toString());
    }
}
