package de.porsche.mtraub.anagogdemoapp;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import org.json.JSONException;
import org.json.JSONObject;

import anagog.pd.service.api.metastate.MetaStateData;
import anagog.pd.service.api.metastate.MetaStateManager;

/**
 * Created by p367151 on 20.02.18.
 */

public class AnagogMetaStateReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals(MetaStateManager.ANAGOG_META_STATE_CHANGED)) {

            JSONObject eventData = new JSONObject();

            try {
                Bundle extras = intent.getExtras();
                if (extras != null) {
                    eventData.put("lat", extras.getDouble("Lat"));
                    eventData.put("long", extras.getDouble("Long"));
                    eventData.put("accuracy", extras.getFloat("Accuracy"));
                    eventData.put("duration", extras.getLong("Duration"));
                }

                MetaStateData data = MetaStateManager.extractMetaState(intent);
                eventData.put("timestamp", data.getTime());
                eventData.put("duration_in_state", data.getDuration());
                eventData.put("meta_state", data.getMetaState().toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }


            RequestPerformer requestPerformer = new RequestPerformer();
            requestPerformer.execute(eventData.toString());
        }
    }
}
