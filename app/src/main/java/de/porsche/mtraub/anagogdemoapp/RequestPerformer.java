package de.porsche.mtraub.anagogdemoapp;

import android.os.AsyncTask;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by p367151 on 21.02.18.
 */

public class RequestPerformer extends AsyncTask<String,Void, Boolean> {

    @Override
    protected Boolean doInBackground(String... strings) {
        try {
            sendPost(strings[0]);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return true;
    }

    private void sendPost(String payload) throws IOException, JSONException {

        String webHookURL = "https://w9s0rbqp2e.execute-api.eu-central-1.amazonaws.com/dev/v1/pva/due-diligence/anagog";
        URL url = new URL(webHookURL);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("POST");
        connection.setDoInput(true);
        connection.setDoOutput(true);
        connection.setUseCaches(false);
        connection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");


        OutputStream os = connection.getOutputStream();
        os.write(payload.getBytes("UTF-8"));
        os.close();

        BufferedReader reader = new BufferedReader(
                new InputStreamReader(connection.getInputStream()));

        for (String line; (line = reader.readLine()) != null; ) {
            System.out.println(line);
        }

        os.close();
        reader.close();

    }
}